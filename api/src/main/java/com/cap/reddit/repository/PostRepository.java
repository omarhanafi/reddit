package com.cap.reddit.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cap.reddit.model.Post;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {
	
	@Query("select p from Post p join p.subreddit s where s.id = :id_subreddit")
	public Page<Post> findAllBySubreddit(@Param("id_subreddit") Long idSubreddit, Pageable pageable);
	
}
