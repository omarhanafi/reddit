package com.cap.reddit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cap.reddit.model.User;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
//	@Query("from Hero where name like :name%")
//	List<Hero> findByName(String name);
	
}
