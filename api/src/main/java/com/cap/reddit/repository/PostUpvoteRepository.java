package com.cap.reddit.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cap.reddit.model.PostUpvote;

@Repository
public interface PostUpvoteRepository extends JpaRepository<PostUpvote, Long> {}
