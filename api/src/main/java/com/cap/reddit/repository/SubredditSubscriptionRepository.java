package com.cap.reddit.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cap.reddit.model.SubredditSubscription;

public interface SubredditSubscriptionRepository extends JpaRepository<SubredditSubscription, Long>{}
