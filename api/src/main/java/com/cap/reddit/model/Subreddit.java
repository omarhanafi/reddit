package com.cap.reddit.model;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "subreddit")
public class Subreddit {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(nullable = false)
	private String name;
	
	private String description;
	
	@Column(nullable = false)
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date date;
	
	@Transient
	private int nbSubscribers = 0;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "subreddit")
	@JsonIgnore
	private List<Post> posts;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "subreddit")
	@JsonIgnore
	private List<SubredditSubscription> subredditSubscriptions;

	public Subreddit() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getNbSubscribers() {
		return nbSubscribers;
	}

	public void setNbSubscribers(int nbSubscribers) {
		this.nbSubscribers = nbSubscribers;
	}

	public List<Post> getPosts() {
		return posts;
	}

	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}

	public List<SubredditSubscription> getSubredditSubscriptions() {
		return subredditSubscriptions;
	}

	public void setSubredditSubscriptions(List<SubredditSubscription> subredditSubscriptions) {
		this.subredditSubscriptions = subredditSubscriptions;
	}

	@Override
	public String toString() {
		return "Subreddit [id=" + id + ", name=" + name + ", description=" + description + ", date=" + date
				+ ", nbSubscribers=" + nbSubscribers + ", posts=" + posts + ", subredditSubscriptions="
				+ subredditSubscriptions + "]";
	}

}
