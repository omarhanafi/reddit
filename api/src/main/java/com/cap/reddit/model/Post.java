package com.cap.reddit.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Formula;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "post")
public class Post implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(nullable = false)
	private String title;
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss.SSS")
	@Column(nullable = false)
	private Timestamp time;
	
	private String content;
	
	@Formula("(select count(*) from post_upvote pu where pu.id_post = id)")
	private int nbUpvotes = 0;
	
	@Formula("(select count(*) from post_comment pc where pc.id_post = id)")
	private int nbComments = 0;
	
	@ManyToOne
	@JoinColumn(name = "id_subreddit")
	private Subreddit subreddit;
	
	@ManyToOne
	@JoinColumn(name = "id_category")
	private Category category;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "post")
	@JsonManagedReference
	private List<PostComment> postComments;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "post")
	@JsonManagedReference
	private List<PostUpvote> postUpvotes;
	
	public Post() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getNbUpvotes() {
		return nbUpvotes;
	}

	public void setNbUpvotes(int nbUpvotes) {
		this.nbUpvotes = nbUpvotes;
	}

	public int getNbComments() {
		return nbComments;
	}

	public void setNbComments(int nbComments) {
		this.nbComments = nbComments;
	}

	public Subreddit getSubreddit() {
		return subreddit;
	}

	public void setSubreddit(Subreddit subreddit) {
		this.subreddit = subreddit;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
	
	public List<PostComment> getPostComments() {
		return postComments;
	}

	public void setPostComments(List<PostComment> postComments) {
		this.postComments = postComments;
	}
	
	public List<PostUpvote> getPostUpvotes() {
		return postUpvotes;
	}

	public void setPostUpvotes(List<PostUpvote> postUpvotes) {
		this.postUpvotes = postUpvotes;
	}

	@Override
	public String toString() {
		return "Post [id=" + id + ", title=" + title + ", time=" + time + ", content=" + content + ", nbUpvotes="
				+ nbUpvotes + ", nbComments=" + nbComments + ", subreddit=" + subreddit + ", category=" + category
				+ ", postComments=" + postComments + ", postUpvotes=" + postUpvotes + "]";
	}

}
