package com.cap.reddit.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cap.reddit.model.User;
import com.cap.reddit.exception.ResourceNotFoundException;
import com.cap.reddit.model.SubredditSubscription;
import com.cap.reddit.repository.UserRepository;

@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	UserRepository repository;

	@GetMapping("/{id}")
	public User getUser(@PathVariable(value = "id") Long id) throws ResourceNotFoundException {
		return repository.findById(id).orElseThrow(() -> new ResourceNotFoundException("User not found id = "+id));
	}
	
	@GetMapping("/{id}/subredditsubscriptions")
	public List<SubredditSubscription> getUserSubredditSubcriptions(@PathVariable(value = "id") Long id) throws ResourceNotFoundException {
		return repository.findById(id).orElseThrow(() -> new ResourceNotFoundException("User not found id = "+id)).getSubredditSubscriptions();
	}
	
}