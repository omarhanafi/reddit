package com.cap.reddit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cap.reddit.exception.ResourceNotFoundException;
import com.cap.reddit.model.Post;
import com.cap.reddit.model.PostComment;
import com.cap.reddit.repository.PostCommentRepository;
import com.cap.reddit.repository.PostRepository;

@RestController
@CrossOrigin
@RequestMapping("/postcomment")
public class PostCommentController {
	
	@Autowired
	PostCommentRepository commentRepository;
	
	@Autowired
	PostRepository postRepository;

	@PostMapping
	public void addPostComment(@RequestBody PostComment comment) {
		this.commentRepository.save(comment);	/* Adding new comment */
	}
	
}
