package com.cap.reddit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cap.reddit.exception.ResourceNotFoundException;
import com.cap.reddit.model.PostUpvote;
import com.cap.reddit.repository.PostUpvoteRepository;

@RestController
@CrossOrigin
@RequestMapping("/postupvote")
public class PostUpvoteController {
	
	@Autowired
	PostUpvoteRepository postUpvoteRepository;

	@PostMapping
	public void addPostUpvote(@RequestBody PostUpvote postUpvote) throws ResourceNotFoundException {
		this.postUpvoteRepository.save(postUpvote);	/* Adding new upvote */
	}
	
	@DeleteMapping("/{id}")
	public void deletePostUpvote(@PathVariable(value = "id") Long id) {
		this.postUpvoteRepository.deleteById(id);
	}
	
	
}
