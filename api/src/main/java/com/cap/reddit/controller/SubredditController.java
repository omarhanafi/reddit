package com.cap.reddit.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cap.reddit.exception.ResourceNotFoundException;
import com.cap.reddit.model.Post;
import com.cap.reddit.model.Subreddit;
import com.cap.reddit.repository.PostRepository;
import com.cap.reddit.repository.SubredditRepository;

@RestController
@CrossOrigin
@RequestMapping("/subreddit")
public class SubredditController {
	
	final int pageSize = 5;
	
	@Autowired
	SubredditRepository subredditRepository;
	
	@Autowired
	PostRepository postRepository;
	
	@GetMapping("{id}")
	public Subreddit getSubreddit(@PathVariable(value = "id") Long id) throws ResourceNotFoundException {
		return subredditRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Subreddit not found id = "+id));
	}

	@GetMapping("/{id}/posts")
	public List<Post> getPosts(@PathVariable(value = "id") Long id, @RequestParam(value = "page", required = true) int page) {
		return postRepository.findAllBySubreddit(id, PageRequest.of(page, pageSize)).getContent();
	}
	
	@GetMapping
	public Subreddit getSubredditsByName(@RequestParam(value = "name", required = true) String name) throws ResourceNotFoundException {
		List<Subreddit> list = subredditRepository.findByName(name);
		if(!list.isEmpty()) {
			return list.get(0);
		} else {
			throw new ResourceNotFoundException("No Subreddit found for name = "+name);
		}		
	}
	
}
