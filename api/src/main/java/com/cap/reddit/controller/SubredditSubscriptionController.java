package com.cap.reddit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cap.reddit.model.SubredditSubscription;
import com.cap.reddit.repository.SubredditSubscriptionRepository;

@RestController
@RequestMapping("/subredditsubscription")
@CrossOrigin
public class SubredditSubscriptionController {
	
	@Autowired
	SubredditSubscriptionRepository repository;
	
	@PostMapping
	public void addSubredditSubscription(@RequestBody SubredditSubscription subredditSubscription) {
		this.repository.save(subredditSubscription);
	}
	
	@DeleteMapping("/{id}")
	public void deleteSubredditSubscription(@PathVariable(value = "id") Long id) {
		this.repository.deleteById(id);
	}

}
