package com.cap.reddit.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cap.reddit.exception.ResourceNotFoundException;
import com.cap.reddit.model.Post;
import com.cap.reddit.model.PostUpvote;
import com.cap.reddit.model.Subreddit;
import com.cap.reddit.repository.PostRepository;
import com.cap.reddit.repository.PostUpvoteRepository;

@RestController
@CrossOrigin
@RequestMapping("/post")
public class PostController {
	
	@Autowired
	PostRepository postRepository;
	
	@Autowired
	PostUpvoteRepository postUpvoteRepository;

	@GetMapping("/{id}")
	public Post getPost(@PathVariable(value = "id") Long id) throws ResourceNotFoundException {
		return this.postRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Post not found id = "+id));
	}
	
	@GetMapping("/{id}/subreddit")
	public Subreddit getPostSubreddit(@PathVariable(value = "id") Long id) throws ResourceNotFoundException {
		return this.postRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Post not found id = "+id)).getSubreddit();
	}
	
}
