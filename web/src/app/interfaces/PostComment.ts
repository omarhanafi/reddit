import { Post } from './Post';
import { User } from './User';

export interface PostComment {
    id : number;
    content : string;
    post : Post;
    time : string;
    user : User;
}