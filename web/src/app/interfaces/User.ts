export interface User {
    id : number;
    username : string;
    password : string;
    karma : number;
    birthday : string;
}