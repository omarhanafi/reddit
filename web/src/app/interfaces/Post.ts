import { Subreddit } from './Subreddit';
import { Category } from './Category';
import { PostComment } from './PostComment';
import { PostUpvote } from './PostUpvote';

export interface Post {
    id : number;
    title : String;
    time : String;
    content : String;
    subreddit : Subreddit;
    category : Category;
    nbComments : number;
    nbUpvotes : number;
    postComments : PostComment[];
    postUpvotes : PostUpvote[];
}