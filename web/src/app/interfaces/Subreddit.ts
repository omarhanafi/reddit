export interface Subreddit {
    id : number;
    name : String;
    date : Date;
    description : String;
}