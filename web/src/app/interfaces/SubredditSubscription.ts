import { Subreddit } from './Subreddit';
import { User } from './User';

export interface SubredditSubscription {
    id : number;
    subreddit : Subreddit;
    user : User;
}