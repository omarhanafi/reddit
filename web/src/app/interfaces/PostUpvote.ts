import { User } from './User';
import { Post } from './Post';

export class PostUpvote {
    id : number;
    user : User;
    post : Post;
}