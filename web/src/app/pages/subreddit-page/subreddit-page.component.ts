import { Component, OnInit } from '@angular/core';
import { Post } from 'src/app/interfaces/Post';
import { PostService } from 'src/app/services/post/post.service';
import { ActivatedRoute } from '@angular/router';
import { Subreddit } from 'src/app/interfaces/Subreddit';
import { SubredditService } from 'src/app/services/subreddit/subreddit.service';

@Component({
  selector: 'app-subreddit-page',
  templateUrl: './subreddit-page.component.html',
  styleUrls: ['./subreddit-page.component.css']
})
export class SubredditPageComponent implements OnInit {

  posts : Post[];
  subreddit : Subreddit;

  constructor(private postService : PostService, private route : ActivatedRoute, private subredditService : SubredditService) {}

  ngOnInit() {
    /* Recovering subreddit */
    this.subredditService.getSubredditByName(this.route.snapshot.paramMap.get('name')).subscribe((data) => {
      this.subreddit = data;
      this.getPosts(0); /* First page */
    })
  }

  getPosts(page : number){
    /* Recovering posts */
    this.postService.getPostsBySubreddit(this.subreddit.id,page).subscribe((data) => {
      data.forEach((value) => {
        value.subreddit = this.subreddit;
      })
      this.posts = data;
    })
  }

}
