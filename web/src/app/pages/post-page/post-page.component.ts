import { Component, OnInit, Input } from '@angular/core';
import { Post } from 'src/app/interfaces/Post';
import { PostComment } from 'src/app/interfaces/PostComment';
import { Subreddit } from 'src/app/interfaces/Subreddit';
import { ActivatedRoute } from '@angular/router';
import { PostService } from 'src/app/services/post/post.service';
import { CommentService } from 'src/app/services/comment/comment.service';

@Component({
  selector: 'app-post-page',
  templateUrl: './post-page.component.html',
  styleUrls: ['./post-page.component.css']
})
export class PostPageComponent implements OnInit {

  post : Post;
  subreddit : Subreddit;

  constructor(private activatedRoute : ActivatedRoute, private postService : PostService, private commentService : CommentService) { }

  ngOnInit() {
    this.fetchPost();
  }

  fetchPost(){
    this.postService.getPost(+this.activatedRoute.snapshot.paramMap.get("id")).subscribe((data) => {  /* Recovering post */
      this.post = data;
      this.fetchSubreddit();
    })
  }

  fetchSubreddit(){
    this.postService.getSubredditByPost(this.post.id).subscribe((data) => {   /* Recovering post's subreddit */
      this.subreddit = data;
      this.post.subreddit = data;
    })
  }

  addComment(value : string){
    let comment : PostComment = {id : null, content : value, post : this.post, time : null, user : JSON.parse(sessionStorage.getItem("user"))};
    this.commentService.addComment(comment).subscribe(() => {
      this.fetchPost();
    });
  }

}