import { Component, OnInit } from '@angular/core';
import { SubredditService } from 'src/app/services/subreddit/subreddit.service';
import { ActivatedRoute } from '@angular/router';
import { Subreddit } from 'src/app/interfaces/Subreddit';

@Component({
  selector: 'app-post-creation-page',
  templateUrl: './post-creation-page.component.html',
  styleUrls: ['./post-creation-page.component.css']
})
export class PostCreationPageComponent implements OnInit {

  subreddit : Subreddit;

  constructor(private route : ActivatedRoute, private subredditService : SubredditService) {}

  ngOnInit() {
    /* Recovering subreddit */
    this.subredditService.getSubredditByName(this.route.snapshot.paramMap.get('name')).subscribe((data) => {
      this.subreddit = data;
    })
  }

}
