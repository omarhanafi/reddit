import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostCreationPageComponent } from './post-creation-page.component';

describe('PostCreationPageComponent', () => {
  let component: PostCreationPageComponent;
  let fixture: ComponentFixture<PostCreationPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostCreationPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostCreationPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
