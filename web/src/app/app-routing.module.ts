import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubredditPageComponent } from './pages/subreddit-page/subreddit-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { PostPageComponent } from './pages/post-page/post-page.component';
import { PostCreationPageComponent } from './pages/post-creation-page/post-creation-page.component';


const routes: Routes = [
  { path : '', redirectTo : 'home', pathMatch : 'full' } ,
  { path : 'home', component : HomePageComponent },
  { path : 'r/:name', component : SubredditPageComponent },
  { path : 'r/:name/submit', component : PostCreationPageComponent },
  { path : 'post/:id', component : PostPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
