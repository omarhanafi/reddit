import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SubredditPageComponent } from './pages/subreddit-page/subreddit-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { PostComponent } from './components/post/post.component';
import { HttpClientModule } from '@angular/common/http';
import { PostPageComponent } from './pages/post-page/post-page.component';
import { CommunityDetailsComponent } from './components/community-details/community-details.component';
import { PostCreationPageComponent } from './pages/post-creation-page/post-creation-page.component';

@NgModule({
  declarations: [
    AppComponent,
    SubredditPageComponent,
    HomePageComponent,
    PostComponent,
    PostPageComponent,
    CommunityDetailsComponent,
    PostCreationPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
