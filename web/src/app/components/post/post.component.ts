import { Component, OnInit, Input } from '@angular/core';
import { Post } from 'src/app/interfaces/Post';
import { PostUpvoteService } from 'src/app/services/post-upvote/post-upvote.service';
import { User } from 'src/app/interfaces/User';
import { PostUpvote } from 'src/app/interfaces/PostUpvote';
import { PostService } from 'src/app/services/post/post.service';
import { Subreddit } from 'src/app/interfaces/Subreddit';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  @Input()
  post : Post;

  postUpvote : PostUpvote;

  constructor(private postUpvoteService : PostUpvoteService, private postService : PostService) { }

  ngOnInit() {
    this.setPostUpvote();
  }

  setPostUpvote(){
    this.postUpvote = null;   /* Resetting the postUpvote attribute */
    let user : User = JSON.parse(sessionStorage.getItem("user"));
    this.post.postUpvotes.forEach((value) => {
      if(value.user.id == user.id){
        this.postUpvote = value;
      }
    });
  }

  refreshPost(){
    this.postService.getPost(this.post.id).subscribe((data) => {
      let subreddit : Subreddit = this.post.subreddit;    /* Saving subreddit data */
      this.post = data;
      this.post.subreddit = subreddit;
      this.setPostUpvote();
    })
  }

  upvoteProcess(){
    if(this.postUpvote){  /* If upvoted, delete */
      this.postUpvoteService.deletePostUpvote(this.postUpvote.id).subscribe(() => {
        this.refreshPost();
      })
    } else {  /* else, create new PostUpvote */
      let user : User = JSON.parse(sessionStorage.getItem("user"));
      let newPostUpvote : PostUpvote = {id : null, user : user, post : this.post};
      this.postUpvoteService.addPostUpvote(newPostUpvote).subscribe(() => {
        this.refreshPost();
      })
    }
  }
}
