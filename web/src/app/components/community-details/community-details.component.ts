import { Component, OnInit, Input } from '@angular/core';
import { Subreddit } from 'src/app/interfaces/Subreddit';
import { SubredditSubscription } from 'src/app/interfaces/SubredditSubscription';
import { SubredditSubscriptionService } from 'src/app/services/subreddit-subscription/subreddit-subscription.service';
import { User } from 'src/app/interfaces/User';

@Component({
  selector: 'app-community-details',
  templateUrl: './community-details.component.html',
  styleUrls: ['./community-details.component.css']
})
export class CommunityDetailsComponent implements OnInit {

  @Input()
  subreddit : Subreddit;

  user : User = JSON.parse(sessionStorage.getItem("user"));

  subredditSubscription : SubredditSubscription;

  constructor(private subredditSubscriptionService : SubredditSubscriptionService) { }

  ngOnInit() {
    this.getSubredditSubscription();
  }

  getSubredditSubscription(){
    this.subredditSubscriptionService.getSubredditSubscriptionsByUser(this.user.id).subscribe((data) => {
      data.forEach((value) => {
        if(value.subreddit.id == this.subreddit.id){
          this.subredditSubscription = value;
        }
      })
    })
  }

  subscriptionProcess(){
    if(this.subredditSubscription){
      this.subredditSubscriptionService.deleteSubredditSubscription(this.subredditSubscription.id).subscribe(() => {
        this.subredditSubscription = null;
      })
    } else {
      let newSubredditSubscription : SubredditSubscription = {id : null, subreddit : this.subreddit, user : this.user};
      this.subredditSubscriptionService.addSubredditSubscription(newSubredditSubscription).subscribe(() => {
        this.getSubredditSubscription();
      })
    }
  }

}
