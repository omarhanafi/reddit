import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { PostComment } from 'src/app/interfaces/PostComment';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  baseUrl : string = environment.apiUrl;
  
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private httpClient : HttpClient) { }

  addComment(comment : PostComment) : Observable<any> {
    return this.httpClient.post(this.baseUrl+"postcomment", comment, this.httpOptions).pipe(catchError(this.handleError));
  }
  
  handleError(error){
    let errorMessage : string = error.error.message;
    return throwError(errorMessage);
  }

}
