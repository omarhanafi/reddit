import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Post } from 'src/app/interfaces/Post';
import { environment } from 'src/environments/environment';
import { catchError, map, tap } from 'rxjs/operators';
import { Subreddit } from 'src/app/interfaces/Subreddit';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  baseUrl : string = environment.apiUrl;

  constructor(private http : HttpClient) {}

  getPostsBySubreddit(idSubreddit : number, page : number) : Observable<Post[]>{
    return this.http.get<Post[]>(this.baseUrl+"subreddit/"+idSubreddit+"/posts?page="+page).pipe(catchError(this.handleError));
  }

  getPost(id : number) : Observable<Post>{
    return this.http.get<Post>(this.baseUrl+"post/"+id).pipe(catchError(this.handleError));
  }

  getSubredditByPost(id : number) : Observable<Subreddit>{
    return this.http.get<Subreddit>(this.baseUrl+"post/"+id+"/subreddit").pipe(catchError(this.handleError));
  }

  handleError(error){
    let errorMessage : string = error.error.message;
    return throwError(errorMessage);
  }

}