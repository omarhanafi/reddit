import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { PostUpvote } from 'src/app/interfaces/PostUpvote';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostUpvoteService {

  baseUrl : string = environment.apiUrl;
  
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http : HttpClient) {}

  addPostUpvote(postUpvote : PostUpvote) : Observable<any>{
    return this.http.post(this.baseUrl+"postupvote", postUpvote, this.httpOptions);
  }

  deletePostUpvote(id : number) : Observable<any>{
    return this.http.delete(this.baseUrl+"postupvote/"+id);
  }

  handleError(error){
    let errorMessage : string = error.error.message;
    return throwError(errorMessage);
  }

}
