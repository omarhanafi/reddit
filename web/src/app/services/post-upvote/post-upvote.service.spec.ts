import { TestBed } from '@angular/core/testing';

import { PostUpvoteService } from './post-upvote.service';

describe('PostUpvoteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PostUpvoteService = TestBed.get(PostUpvoteService);
    expect(service).toBeTruthy();
  });
});
