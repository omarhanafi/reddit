import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { throwError, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { SubredditSubscription } from 'src/app/interfaces/SubredditSubscription';

@Injectable({
  providedIn: 'root'
})
export class SubredditSubscriptionService {

  baseUrl : string = environment.apiUrl;

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http : HttpClient) { }

  getSubredditSubscriptionsByUser(userId : number) : Observable<SubredditSubscription[]> {
    return this.http.get<SubredditSubscription[]>(this.baseUrl+"user/"+userId+"/subredditsubscriptions").pipe(catchError(this.handleError));
  }

  addSubredditSubscription(subredditsubscription : SubredditSubscription) : Observable<any> {
    return this.http.post(this.baseUrl+"subredditsubscription", subredditsubscription, this.httpOptions);
  }

  deleteSubredditSubscription(id : number) : Observable<any> {
    return this.http.delete(this.baseUrl+"subredditsubscription/"+id);
  }

  handleError(error){
    let errorMessage : string = error.error.message;
    return throwError(errorMessage);
  }
}
