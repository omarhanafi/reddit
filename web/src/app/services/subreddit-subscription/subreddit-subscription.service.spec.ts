import { TestBed } from '@angular/core/testing';

import { SubredditSubscriptionService } from './subreddit-subscription.service';

describe('SubredditSubscriptionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SubredditSubscriptionService = TestBed.get(SubredditSubscriptionService);
    expect(service).toBeTruthy();
  });
});
