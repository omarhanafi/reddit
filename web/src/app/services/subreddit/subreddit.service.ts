import { Injectable } from '@angular/core';
import { Subreddit } from 'src/app/interfaces/Subreddit';
import { Observable, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { catchError, map, tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class SubredditService {

  baseUrl : string = environment.apiUrl;

  constructor(private httpClient : HttpClient) { }

  getSubreddit(id : number) : Observable<Subreddit> {
    return this.httpClient.get<Subreddit>(this.baseUrl+"subreddit/"+id).pipe(catchError(this.handleError));
  }

  getSubredditByName(name : String) : Observable<Subreddit> {
    return this.httpClient.get<Subreddit>(this.baseUrl+"subreddit?name="+name).pipe(catchError(this.handleError));
  }

  handleError(error){
    let errorMessage : string = error.error.message;
    return throwError(errorMessage);
  }

}
