import { Injectable } from '@angular/core';
import { User } from 'src/app/interfaces/User';

@Injectable({
  providedIn: 'root'
})
export class SecurityService {

  constructor() { }

  isLoggedIn() : boolean {
    let user : User = JSON.parse(sessionStorage.getItem("user"));
    if(user === null){
      return false;
    } else {
      return true;
    }
  }
  
}
