import { Component } from '@angular/core';
import { User } from './interfaces/User';
import { SecurityService } from './services/security/security.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private securityService : SecurityService){}

  ngOnInit() {
    let user : User = {id : 1, username : "omar", password : "omar", karma : 0, birthday : "1997-05-28"};
    sessionStorage.setItem("user", JSON.stringify(user));
  }

}
